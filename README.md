# Nepdict-website
<p align="center">
<img src="https://github.com/nirooj56/Nepdict-website/blob/master/index.png" alt="Nepdict">
</p>

# Credits:
* The Awesome Bootstrap Table used for Dictionary was made with Love by <a href="http://www.creative-tim.com/" target="_blank">Creative Tim</a>.
* The credit for Unicode conversion goes to <a href="http://www.nepali-unicode.com" target="_blank"> Nepali Unicode</a> Team.
